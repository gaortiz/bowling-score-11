package com.jobsity.challenge.model;

import com.jobsity.challenge.enums.ScoreType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FrameShots {

    private List<String> shots;

    protected FrameShots() {
        this.shots = new ArrayList<>();
    }

    public static FrameShots createFrameShot() {
        return new FrameShots();
    }

    public void addStrikeFrame() {
        this.shots.addAll(Arrays.asList(ScoreType.EMPTY_SHOT.label, ScoreType.STRIKE.label));
    }

    private void addStrikeForLastFrame() {
        this.shots.add(ScoreType.STRIKE.label);
    }

    public void addSpareFrame(Shot shot) {
        this.shots.addAll(Arrays.asList(shot.getName(), ScoreType.SPARE.label));
    }

    private void addSpareForLastFrame() {
        this.shots.add(ScoreType.SPARE.label);
    }

    public void addNormalFrame(List<Shot> shots) {
        this.shots.addAll(shots.stream().map(Shot::getName).collect(Collectors.toList()));
    }

    private void addOneShot(Shot shot) {
        this.shots.add(shot.getName());
    }

    public void addLastFrame(List<Shot> shots) {
        setLastFrameFirstShot(shots.get(0));
        setLastFrameShot(shots.get(1), shots.get(0));
        setLastFrameShot(shots.get(2), shots.get(1));
    }

    private void setLastFrameFirstShot(Shot shot) {
        if (shot.isStrike()) {
            addStrikeForLastFrame();
        } else {
            addOneShot(shot);
        }
    }

    private void setLastFrameShot(Shot shot, Shot previousShot) {
        if (shot.isStrike() && !previousShot.isSpare(shot)) addStrikeForLastFrame();
        else if (previousShot.isSpare(shot) && !previousShot.isStrike()) {
            addSpareForLastFrame();
        } else {
            addOneShot(shot);
        }
    }

    public List<String> getShots() {
        return shots;
    }

    @Override
    public String toString() {
        return "FrameShots{" +
                "shots=" + shots +
                '}';
    }
}
